---

- hosts: localhost
  gather_facts: no 
  connection: local
  tags: [CSV]
  vars:
    spreadsheet: '{{ playbook_dir }}/template.xls'  
    dest: '{{ playbook_dir }}/'

  tasks:
    - name: Create CSV from XLS
      xls_to_csv:
        src: '{{ spreadsheet }}' 
        dest: '{{ dest }}' 
        sheets: 
          - 'TENANT'
          - 'APP_PROFILE'
          - 'L3'
          - 'EPG'
          - 'ATTACHIFCE'
          - 'VRF'
        warn: True

    - debug:
        msg: 'File {{ dest }}{{ item }}.csv has been created'
      loop: '{{ sheet_filenames }}'

- hosts: localhost
  gather_facts: no
  tags: [CSV]

  tasks:
    - name: Export Tenant Info
      csv_to_facts:
        src: '{{ playbook_dir }}/TENANT.csv'
      register: tenant

- hosts: localhost
  gather_facts: no
  tags: [CSV]

  tasks:
    - name: Export APP PROFILE Info
      csv_to_facts:
        src: '{{ playbook_dir }}/APP_PROFILE.csv'
      register: appprofile

- hosts: localhost
  gather_facts: no
  tags: [CSV]

  tasks:
    - name: Export L3 Info
      csv_to_facts:
        src: '{{ playbook_dir }}/L3.csv'
      register: l3

- hosts: localhost
  gather_facts: no
  tags: [CSV]

  tasks:
    - name: Export EPG Info
      csv_to_facts:
        src: '{{ playbook_dir }}/EPG.csv'
      register: epg

- hosts: localhost
  gather_facts: no
  tags: [CSV]

  tasks:
    - name: Export Attach Interface Info
      csv_to_facts:
        src: '{{ playbook_dir }}/ATTACHIFCE.csv'
      register: attachifce

- hosts: localhost
  gather_facts: no
  tags: [CSV]

  tasks:
    - name: Export VRF Info
      csv_to_facts:
        src: '{{ playbook_dir }}/VRF.csv'
      register: vrf

- name: "Configure ACI Tenant part"
  hosts: ACI
  gather_facts: no 
  connection: local
  serial: 4
  vars:
    tenant: "{{ hostvars['localhost']['tenant']['ansible_facts']['spreadsheet'] }}"
    appprofile: "{{ hostvars['localhost']['appprofile']['ansible_facts']['spreadsheet'] }}"
    l3: "{{ hostvars['localhost']['l3']['ansible_facts']['spreadsheet'] }}"
    epg: "{{ hostvars['localhost']['epg']['ansible_facts']['spreadsheet'] }}"
    attachifce: "{{ hostvars['localhost']['attachifce']['ansible_facts']['spreadsheet'] }}"
    vrf: "{{ hostvars['localhost']['vrf']['ansible_facts']['spreadsheet'] }}"
  tasks:
    # - debug:
    #     msg:
          # - '{{ tenant }}'
          # - '{{ appprofile }}'
          # - '{{ l3 }}'
          # - '{{ epg }}'
          # - '{{ attachifce }}'
          # - '{{ vrf }}'
    - name: Create ACI Tenant
      aci_tenant:
        host: '{{ ansible_host }}'
        username: '{{ vault_user }}' 
        password: '{{ vault_password }}'
        tenant: '{{ item.TENANT }}'
        description: 'Tenant Cisco Community {{ item.TENANT }}'
        state: 'present'
        validate_certs: 'no'
      tags: tenant
      loop: '{{ tenant }}'

    - name: Create Application Profile 
      aci_ap:
        host: '{{ ansible_host }}'
        username: '{{ vault_user }}' 
        password: '{{ vault_password }}'
        tenant: '{{ item.TENANT }}'
        ap: '{{ item.APP_PROFILE }}'
        description: 'Application Profile {{ item.APP_PROFILE }}'
        state: present
        validate_certs: 'no'
      tags: app
      loop: '{{ appprofile }}'

    - name: Add a new VRF 
      aci_vrf:
        host: '{{ ansible_host }}'
        username: '{{ vault_user }}' 
        password: '{{ vault_password }}'
        vrf: '{{ item.VRF }}'
        tenant: '{{ item.TENANT }}'
        descr: 'Create VRF {{ item.VRF }} in {{ item.TENANT }}'
        policy_control_preference: unenforced
        policy_control_direction: ingress
        state: present
        validate_certs: 'no'
      tags: l3
      loop: '{{ vrf }}'

    - name: Enable Preferred Group on VRF using aci rest api
      aci_rest:
        host: '{{ ansible_host }}'
        username: '{{ vault_user }}' 
        password: '{{ vault_password }}'
        validate_certs: 'no'
        method: post
        path: /api/node/mo/uni/tn-{{ item.TENANT }}/ctx-{{ item.VRF }}/any.json
        content:
          vzAny:
            attributes:
              dn: "uni/tn-{{ item.TENANT }}/ctx-{{ item.VRF }}/any"
              prefGrMemb: "enabled"
      tags: l3
      loop: '{{ vrf }}'

    - name: Create Bridge Domain CSC BD
      aci_bd:
        host: "{{ ansible_host }}"
        username: "{{ vault_user }}"
        password: "{{ vault_password }}"
        validate_certs: no
        tenant: '{{ item.TENANT }}'
        bd: '{{ item.BD }}'
        vrf: '{{ item.VRF }}'
        enable_multicast: 'yes'
        enable_routing: 'yes'
        l2_unknown_unicast: 'flood'
        l3_unknown_multicast: 'flood'
        multi_dest: 'bd-flood'
        arp_flooding: 'yes'
        state: present
      tags: l3
      loop: '{{ l3 }}'

    - name: Modify Bridge Domain 
      aci_rest:
        host: '{{ ansible_host }}'
        username: '{{ vault_user }}' 
        password: '{{ vault_password }}'
        validate_certs: 'no'
        method: post
        path: /api/node/mo/uni/tn-{{ item.TENANT }}/BD-{{ item.BD }}.json
        content:
          fvBD:
            attributes:
              dn: "uni/tn-{{ item.TENANT }}/BD-{{ item.BD }}"
              hostBasedRouting: "true"
      tags: l3
      loop: '{{ l3 }}'

    - name: Create Bridge Domain Subnet 
      aci_bd_subnet:
        host: "{{ ansible_host }}"
        username: "{{ vault_user }}"
        password: "{{ vault_password }}"
        validate_certs: no
        tenant: '{{ item.TENANT }}'
        bd: '{{ item.BD }}'
        gateway: "{{ (item.BD_SUBNET).split('/')[0] }}"
        mask: "{{ (item.BD_SUBNET).split('/')[1] | int }}"
        scope: public
        state: present
      tags: l3
      loop: '{{ l3 }}'

    - name: Create EPG
      aci_epg:
        host: "{{ ansible_host }}"
        username: "{{ vault_user }}"
        password: "{{ vault_password }}"
        validate_certs: no
        tenant: '{{ item.TENANT }}'
        ap: '{{ item.APP_PROFILE }}'
        epg: '{{ item.EPG }}'
        description: 'Cisco Community demo EPG {{ item.EPG }}'
        bd: '{{ item.BD }}'
        preferred_group: yes
        state: present
      tags: epg
      loop: '{{ epg }}'

    - name: Add physical domain to EPG 
      aci_epg_to_domain:
        host: "{{ ansible_host }}"
        username: "{{ vault_user }}"
        password: "{{ vault_password }}"
        validate_certs: no
        tenant: '{{ item.TENANT }}'
        ap: '{{ item.APP_PROFILE }}'
        epg: '{{ item.EPG }}'
        domain: '{{ item.PHYSICALDOMAIN }}'
        domain_type: phys
        state: present
      tags: epg
      loop: '{{ epg }}'

    - name: Attach VPC to EPG 
      when: 
          - "'{{ item.PORT_TYPE }}' == 'VPC'"
      aci_static_binding_to_epg:
        host: "{{ ansible_host }}"
        username: "{{ vault_user }}"
        password: "{{ vault_password }}"
        validate_certs: no
        tenant: '{{ item.TENANT }}'
        ap: '{{ item.APP_PROFILE }}'
        epg: '{{ item.EPG }}'
        encap_id: '{{ item.VLAN }}'
        deploy_immediacy: immediate
        interface_mode: '{{ item.MODE }}'
        interface_type: vpc
        pod_id: 1
        leafs: '{{ item.LEAF }}'
        interface: '{{ item.PORT }}'
        state: present
      tags: epg
      loop: '{{ attachifce }}'

    - name: Attach Standard Interface to EPG
      when: 
          - "'{{ item.PORT_TYPE }}' == 'STD'"
      aci_static_binding_to_epg:
        host: "{{ ansible_host }}"
        username: "{{ vault_user }}"
        password: "{{ vault_password }}"
        validate_certs: no
        tenant: '{{ item.TENANT }}'
        ap: '{{ item.APP_PROFILE }}'
        epg: '{{ item.EPG }}'
        encap_id: '{{ item.VLAN }}'
        deploy_immediacy: immediate
        interface_mode: '{{ item.MODE }}'
        interface_type: switch_port
        pod_id: 1
        leafs: '{{ item.LEAF }}'
        interface: '{{ item.PORT }}'
        state: present
      tags: epg
      loop: '{{ attachifce }}'










