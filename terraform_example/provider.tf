provider "aci" {
  username = var.username
  password = var.password
  private_key = var.private_key
  cert_name = var.cert_name
  url      = var.url
  insecure = true
}
